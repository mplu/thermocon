#!/bin/sh

DATE=$(date +%Y-%m-%d)
HEURE=$(date +%H:%M:%S) 
TEMP_INDEX_FILE_NAME='tempindex.html'
METEO_FILE="myplace.json"

USER="user"

cd /home/$USER/dev_soft/thermocon/

#################### getting data from DHT22/esp32 web server #############

#del previous file
if [ -f $TEMP_INDEX_FILE_NAME ];then
	echo "file exists ! suppress it";
	rm $TEMP_INDEX_FILE_NAME*
else
	echo "file does not exist"
fi

#dummy request to refresh temp
wget http://192.168.1.106 -O /dev/null
sleep 2

#request html file
wget http://192.168.1.106 -O  $TEMP_INDEX_FILE_NAME

#grab data
TEMPERATURE1=`xmllint --html --xpath '//span[@id="temperature"]/text()' $TEMP_INDEX_FILE_NAME 2> /dev/null`
HUMIDITY1=`xmllint --html --xpath '//span[@id="humidity"]/text()' $TEMP_INDEX_FILE_NAME 2> /dev/null`

echo $DATE
echo $HEURE
echo $TEMPERATURE1
echo $HUMIDITY1


#################### getting JSON file from Openweathermap web server #############
if [ -f $METEO_FILE ];then
	echo "file exists ! suppress it";
	rm $METEO_FILE*
else
	echo "file does not exist"
fi

wget --no-check-certificate "https://api.openweathermap.org/data/2.5/weather?id=6617651&appid=XXXXXMY_API_KEYXXXXX" -O $METEO_FILE

#################### Prepare display #############
#choose random background
FOND_FILE=`ls fond | shuf -n 1`
cp fond/"$FOND_FILE" fond.bmp

#call python script with data grabbed from ESP32
python thermocon.py $TEMPERATURE1 $HUMIDITY1

#refresh screen
#https://learn.adafruit.com/adafruit-pitft-28-inch-resistive-touchscreen-display-raspberry-pi/easy-install-2
sudo kill -9 `pgrep fbi`
sudo fbi -T 2 -d /dev/fb1 -noverbose -a out.jpg
cp out.jpg /media/thermo/
