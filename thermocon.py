#!/usr/bin/python
# -*- coding: utf-8 -*-

"""ThermoCon
   Thermometre electronique qui receupere
   des infos a droite a gauche
"""

import json
import sys
from PIL import Image 
from PIL import ImageDraw 
from PIL import ImageFont 
import glob
import time
from datetime import datetime

######### get esp32 data via parameter ##############
for i in range(1, len(sys.argv)):
    print('argument:', i, 'value:', sys.argv[i])
temperature_ch1 = sys.argv[1]
humidity_ch1    = sys.argv[2]

######### Opening JSON file ##############
with open('myplace.json') as f:
    data = list(json.load(f).items())
#print(data)
print("")
#get temperature in kelvin
temperature = data[3][1]["temp"]
#convert in °C
temperature = temperature - 273.15
print(str(round(temperature,1)) + "°C")

#get humidity in % HR
humidity = data[3][1]["humidity"]
print(str(round(humidity,1)) + "%")

#get wind in m/s
wind = data[5][1]["speed"]
#convert in km/h
wind = wind * 3.6
print(str(round(wind,1)) + "km/h")

print(temperature_ch1 + "°C")
print(humidity_ch1 + "%")

######### Opening DS18B20 data ##############
# https://learn.adafruit.com/adafruits-raspberry-pi-lesson-11-ds18b20-temperature-sensing?view=all
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

print(str(round(read_temp(),1)) + "°C DS18B20")

######### Creating img ##############
img = Image.open('fond.bmp') 
myFont = ImageFont.truetype("DejaVuSans.ttf", 20)



numjour = datetime.today().weekday()
nummois = int(datetime.today().strftime('%m'))
jour = datetime.today().strftime('%d')
if numjour == 0:
    nomjour = "Lundi"
elif numjour == 1:
    nomjour = "Mardi"
elif numjour == 2:
    nomjour = "Mercredi"
elif numjour == 3:
    nomjour = "Jeudi"
elif numjour == 4:
    nomjour = "Vendredi"
elif numjour == 5:
    nomjour = "Samedi"
elif numjour == 6:
    nomjour = "Dimanche"
    
if nummois == 1:
    nommois = "Janvier"
elif nummois == 2:
    nommois = "Février"
elif nummois == 3:
    nommois = "Mars"
elif nummois == 4:
    nommois = "Avril"
elif nummois == 5:
    nommois = "Mai"
elif nummois == 6:
    nommois = "Juin"
elif nummois == 7:
    nommois = "Juillet"
elif nummois == 8:
    nommois = "Aout"
elif nummois == 9:
    nommois = "Septembre"
elif nummois == 10:
    nommois = "Octobre"
elif nummois == 11:
    nommois = "Novembre"
elif nummois == 12:
    nommois = "Décembre"
print(nomjour + jour + nommois)

I1 = ImageDraw.Draw(img) 
baseX = 1
baseY = 1  
I1.text((baseX, baseY), nomjour + " " + jour + " " + nommois, font=myFont, fill =(255, 255, 255))
I1.text((baseX+250,baseY+26), datetime.now().strftime('%Hh%M'), font=myFont, fill =(255, 255, 255))

#external 
baseY = baseY + 70
myFont = ImageFont.truetype("DejaVuSans.ttf", 22)
I1.text((baseX, baseY), "Ext  :", font=myFont, fill =(255, 255, 255))
myFont = ImageFont.truetype("DejaVuSans.ttf", 32)
I1.text((baseX + 75, baseY - 4),str(round(temperature,1)) + "°C", font=myFont, fill =(255, 255, 255))
myFont = ImageFont.truetype("DejaVuSans.ttf", 20)
I1.text((baseX + 200, baseY - 12),str(round(humidity,1)) + "%", font=myFont, fill =(255, 255, 255))
I1.text((baseX + 200, baseY + 10),str(round(wind,1)) + "km/h", font=myFont, fill =(255, 255, 255))

#living room 
baseY = baseY + 55
myFont = ImageFont.truetype("DejaVuSans.ttf", 22)
I1.text((baseX, baseY), "Salon:", font=myFont, fill =(255, 255, 255))
myFont = ImageFont.truetype("DejaVuSans.ttf", 32)
I1.text((baseX + 75, baseY - 4 ),str(round(read_temp(),1)) + "°C", font=myFont, fill =(255, 255, 255))

#room 1
baseY = baseY + 55
myFont = ImageFont.truetype("DejaVuSans.ttf", 22)
I1.text((baseX, baseY), "Ch Mr:", font=myFont, fill =(255, 255, 255))
myFont = ImageFont.truetype("DejaVuSans.ttf", 32)
I1.text((baseX + 75, baseY - 4),str(round(float(temperature_ch1),1)) + "°C", font=myFont, fill =(255, 255, 255))
myFont = ImageFont.truetype("DejaVuSans.ttf", 20)
I1.text((baseX + 200, baseY),str(round(float(humidity_ch1),1)) + "%", font=myFont, fill =(255, 255, 255))
  
#img.show() 
  
img.save("out.jpg")